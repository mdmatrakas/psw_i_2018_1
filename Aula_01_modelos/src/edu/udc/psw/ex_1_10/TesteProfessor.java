package edu.udc.psw.ex_1_10;

import java.util.Scanner;

public class TesteProfessor {

	public static void main(String[] args) {
		//usarProfessor1();
		
		Professor professor = new Professor();
//		usarProfessor2(professor);
//		usarProfessor2(professor);
		lerDados(professor);
		System.out.println("O objeto \'professor\':");
		mostrarDados(professor);
		
		Professor p = new Professor(professor.nome, professor.dataNascimento,
				professor.codigo);
		System.out.println("O objeto \'p\':");
		mostrarDados(p);
	}

	public static void usarProfessor1() {
		Professor professor = new Professor();

		mostrarDados(professor);

		lerDados(professor);

		mostrarDados(professor);
	}
	
	public static void usarProfessor2(Professor professor) {
		

		mostrarDados(professor);

		lerDados(professor);

		mostrarDados(professor);
	}
	
	static void lerDados(Professor professor){
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Informe o nome do professor:");
		professor.nome = sc.nextLine();
		System.out.println("Informe a data de nascimento do professor:");
		professor.dataNascimento = sc.nextLine();
		System.out.println("Informe o codigo do professor:");
		professor.codigo = sc.nextInt();sc.nextLine();
		System.out.println("Informe o nome da disciplina:");
		professor.disciplina = sc.nextLine();
		System.out.println("Informe a titulacao do professor:");
		professor.titulacao = sc.nextLine();
	}
	
	static void mostrarDados(Professor professor){
		System.out.println("O nome do professor �: " + professor.nome);
		System.out.println("O professor nasceu em " + professor.dataNascimento);
		System.out.println("O professor est� registrado com o c�digo: " + professor.codigo);
		System.out.println("O professor leciona a disciplina: " + professor.disciplina);
		System.out.println("A titula��o do professor �: " + professor.titulacao);
	}

}
